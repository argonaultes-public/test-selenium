import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
import os
import unittest

class TestEpsiWebsite(unittest.TestCase):

    def test_display_formation_firefox(self):
        options = webdriver.FirefoxOptions()
        driver = webdriver.Remote(command_executor = os.environ['MYSE_REMOTE'], options = options)
        driver.get("https://epsi.fr")
        menu = driver.find_element(by = By.ID, value = "menu-item-1002")
        ActionChains(driver)\
            .move_to_element(menu)\
            .perform()
        time.sleep(5)
        driver.find_element(by = By.LINK_TEXT, value = "Panorama des formations").click()
        driver.quit()    

    def test_display_formation_chrome(self):
        options = webdriver.ChromeOptions()
        driver = webdriver.Remote(command_executor = os.environ['MYSE_REMOTE'], options = options)
        driver.get("https://epsi.fr")
        menu = driver.find_element(by = By.ID, value = "menu-item-1002")
        ActionChains(driver)\
            .move_to_element(menu)\
            .perform()
        time.sleep(5)
        driver.find_element(by = By.LINK_TEXT, value = "Panorama des formations").click()
        driver.quit()

if __name__ == '__main__':
    unittest.main()